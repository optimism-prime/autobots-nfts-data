import asyncio
import json
from pathlib import Path

import httpx

base_url_json = "https://purple-necessary-salamander-572.mypinata.cloud/ipfs/QmSt83tbx86NTGx7m1z97enYeoqDPjHPzeMJvuETdWLBNJ/"
base_url_png_1 = "https://purple-necessary-salamander-572.mypinata.cloud/ipfs/QmQEWA2GMK6xJdxG9dtxEJnvvtcyQa5jxYu6tYQdKjCWv1/"  # [1, 697]
base_url_png_2 = "https://purple-necessary-salamander-572.mypinata.cloud/ipfs/QmPhwP4iEALGdNxJjQNAY4WQjTLkFK2PvrHYHyPMJB5QF4/"  # [688, 892]
base_url_png_3 = "https://purple-necessary-salamander-572.mypinata.cloud/ipfs/Qme4z95CG1EoL9Ti3JE6D9LBXu3HDfAEwWjjEnVpFSeG4Y/"  # [893, 1004]
out_data_dir = "data/out"

list_goldens = True
fetch_metadata = False
fetch_pngs = False

legendaries = [768, 940, 512, 256]


async def main() -> None:
    if list_goldens:
        golden_ids = []
        for f in Path(f"{out_data_dir}/json/").iterdir():
            obj = json.loads(f.read_text())
            assert obj["attributes"][1]["trait_type"] == "Pattern"
            if int(f.stem) in legendaries:
                continue
            if obj["attributes"][1]["value"] == "Golden":
                golden_ids.append(int(f.stem))
        print(len(golden_ids))
        print(sorted(golden_ids))

    if fetch_metadata:
        for i in range(1, 1005):
            r = httpx.get(f"{base_url_json}{i}.json")
            r.raise_for_status()
            Path(f"{out_data_dir}/json/{i}.json").write_text(
                json.dumps(r.json(), indent=2), encoding="utf-8", newline="\n"
            )

    if fetch_pngs:
        for i in range(1, 1005):
            base_url_png = (
                base_url_png_1
                if 1 <= i < 688
                else base_url_png_2
                if 688 <= i < 893
                else base_url_png_3
            )
            r = httpx.get(f"{base_url_png}{i}.png")
            r.raise_for_status()
            Path(f"{out_data_dir}/png/{i}.png").write_bytes(r.content)


if __name__ == "__main__":
    asyncio.run(main())
